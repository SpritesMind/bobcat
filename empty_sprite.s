     .global         _empty_sprite

.segment "RODATA"
empty_bitmap:
        ; line 0
        .byte   3           ;offset to next line
        .byte   %10000000   ; 1 --> literal
                            ; 0000 --> number of pixels - 1 (so here we have a 1 pixel line)
                            ; 0 --> color index if pixel 0
                            ; 00 -> fill byte
        .byte   %00000000   ;
        ; line 1
        .byte   $0       ; end of sprite

.segment "DATA"
_empty_sprite:                                  ; SCB_REHV_PAL
        .byte   %00000001                       ; TYPE_BACKGROUND | BPP_1 
        .byte   %00010000                       ; HV (reload pal)
        .byte   %00100000                       ; no collide
        .addr   0,empty_bitmap
        .word   0                               ; x = 0
        .word   0                               ; y = 0
        .word   $a000                           ; w = 160 (1*160)
        .word   $6600                           ; h = 102 (1*102)
        .byte   $01                             ; pen 0 - COLOR 0
                                                ; pen 1 - COLOR 1