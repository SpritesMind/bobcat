#include <bobcat.h>


extern SCB_REHV empty_sprite;

long joyPrevious, joyChange, joyCurrent;

//Function for filling the registers that represent the color palette
void bobcat_setpalette(const int* palette){
    unsigned char index;
    
    for(index = 0; index < 16; index++){
        POKE(0xFDA0 + index, palette[index]>>8); //GREEN 4bits
        POKE(0xFDB0 + index, palette[index] & 0xFF); //BLUERED 4+4bits
    }
};

//Install drivers and initial colors for text
void bobcat_initialize(){
    tgi_install(&tgi_static_stddrv);
    tgi_init();
    
    //joy
    joy_install(&joy_static_stddrv);
    joyPrevious = 0;
    joyChange = 0;


    //vdp
	tgi_setpalette(tgi_getdefpalette());
	bobcat_clearScreen();       //draw it to mask any boot garbage
	bobcat_setTextBgColor(0);   //transparent
	bobcat_setTextColor(0xF);   
        
    //sound
    lynx_snd_init();
    lynx_snd_pause();


	CLI();
}

void bobcat_joyupdate(){
    joyPrevious = joyCurrent;
    joyCurrent = joy_read(JOY_1);
    joyChange = (joyPrevious ^ joyCurrent) & joyCurrent;
}


void bobcat_clearScreen(){
    tgi_sprite(&empty_sprite);
}

void bobcat_shakeScreenUpdate()
{
    //thanks Igor !!
    SUZY.hoff = 2 - rand() % 4;
    SUZY.voff = 2 - rand() % 4;
}
void bobcat_shakeScreenReset()
{
    SUZY.hoff = 0;
    SUZY.voff = 0;
}