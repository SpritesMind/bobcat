#ifndef _BOBCAT_H
#define _BOBCAT_H


#include <stdlib.h>
#include <6502.h>
#include <lynx.h>
#include <tgi.h>
#include <conio.h>
#include <peekpoke.h>
#include <joystick.h>
#include <_suzy.h>
#include <_mikey.h>

#define TRUE    1
#define FALSE   0

#define SCREEN_WIDTH    160
#define SCREEN_HEIGHT   102

#define HIDDEN_X        300
#define LAST_SPRITE     0
#define NO_DATA         0
#define SCALE_x1        0x0100
#define SCALE_x2        0x0200

#define STANDARD_PENS   { 0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef }

extern long joyPrevious, joyChange, joyCurrent;
extern SCB_REHV empty_sprite;

void bobcat_initialize();
void bobcat_clearScreen();
void bobcat_setpalette(const int* palette);
#define bobcat_setTextColor(x)  tgi_setcolor((x))
#define bobcat_setTextBgColor(x)  tgi_setbgcolor((x))
void bobcat_shakeScreenUpdate();
void bobcat_shakeScreenReset();

#define bobcat_joyIsPressed(x)  ((joyCurrent & (x)) !=0 )
void bobcat_joyupdate();

#define bobcat_isSpriteEnable(x)  (((x).sprctl1 & SKIP) == 0)
#define bobcat_enableSprite(x)  ((x).sprctl1 &= ~SKIP)
#define bobcat_disableSprite(x) ((x).sprctl1 |= SKIP)
#define bobcat_resetSprite(x)   (x).sprctl0 =  BPP_4 | TYPE_NONCOLL | SKIP;\
                                (x).data = NO_DATA;\
                                (x).hpos = HIDDEN_X;\
                                (x).vpos = 0;\
                                (x).hsize = SCALE_x1;\
                                (x).vsize = SCALE_x1;


#define bobcat_isFlippedH(x)  ((x).sprctl0 & HFLIP)
#define bobcat_flipH(x)  ((x).sprctl0 |= HFLIP)
#define bobcat_unflipH(x)  ((x).sprctl0 &= ~HFLIP)
#define bobcat_toggleFlipH(x)  ((x).sprctl0 ^= HFLIP)

//based on https://atarigamer.com/lynx/lnxhdrgen and Karry header
#define LNX_HEADER_FULL(name, manufacturer, bank0BlockSize, bank1BlockSize, vertical, audioin, eepromsetup)  _Pragma("data-name (push, \"EXEHDR\")");\
    struct  {\
        char magic[4];\
        unsigned short  _bank0BlockSize;    /* bank 0 page size */\
        unsigned short  _bank1BlockSize;    /* bank 1 page size */\
        unsigned short  headerVersion;      /* LNX header version (Handy only accepts version 1) */\
        char _name[32];                     /* 32 bytes cart name */\
        char _manufacturer[16];             /* 16 bytes manufacturer */ \
        unsigned char _vertical;            /* vertical rotation (1=left, 2=right) */\
        unsigned char _audioin;             /* AUDIN used (1) */\
        unsigned char _eepromSetup;         /* eeprom -> use 1 for the most common eeprom, 64 to use SD Cart saves, 65 for both\
                                               eeprom [2:0] -\
                                                ; 0 - no eeprom\
                                                ; 1 - 93c46 16 bit mode (used in Ttris, SIMIS, Alpine Games, ..., MegaPak I at least)\
                                                ; 2      56\
                                                ; 3 - 93c66 16 bit mode\
                                                ; 4      76\
                                                ; 5 - 93c86 16 bit mode\
                                                ; (remark: size in bits is 2^(value+9) -- (please recheck!)\
                                               eeprom [3-5] - reserved - keep it to 0 for further usage\
                                               eeprom [6] - 0 - real eeprom, 1 - eeprom as a file in /saves/flappy.hi on SD cart\
                                               eeprom [7] - 0 - 16 bit mode, 1 - 8 bit mode */\
    } lnxheader = {\
        "LYNX",\
        bank0BlockSize,\
        bank1BlockSize,\
        1,\
        name,\
        manufacturer,\
        vertical,\
        audioin,\
        eepromsetup\
    };\
     _Pragma("data-name (pop)");

#define LNX_HEADER(name, manufacturer)    LNX_HEADER_FULL(name, manufacturer, 1024, 0, 0, 0, 0)


#endif
